# Telegram Basic Bot
[API Documentation](https://azhao12345.github.io/telegram-basic-bot/)

This is a basic telegram bot wrapper that has no dependencies and will run on basically any node version.  This is great for anyone 

## Install
Just copy telegram.js into your project.  There are no dependencies, and you'll definitely be getting the source code right here.

Alternatively, use npm (not recommended)
```bash
npm install --save telegram-basic-bot
```

## Usage
The basic bot uses the EventEmitter pattern when getting events, and standard error first callbacks
```js
var TelegramClient = require('./telegram');
var telegram = new TelegramClient(token);

// send a message
telegram.request('sendMessage', { chat_id: '@mychannel', text: 'hello world' });

// get a file
telegram.requst('getFile', { file_id: fileId }, function(err, file) {
    // do whatever
});

// polling or webhook
telegram.poll(['message']);

// not implemented yet
telegram.listen(['message']);
```
### Named methods
As a convenience, named methods can be generated to shorten calling.  This is not recommended as there is no guarantee that these methods will be kept up to date with new API additions.  If using this feature, methods.js is also required.
```js
var telegram = new TelegramClient(token, true);

telegram.getMe({}, function(err, data) {
    console.log(data);
});
```

## Promises
Fuck off
